﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="20008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">RekuperatorNotifications.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../RekuperatorNotifications.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.IsInterface" Type="Bool">true</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="Rekuperator All Data.vi" Type="VI" URL="../Rekuperator All Data.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+A!!!!%A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!'B!=!!?!!""(F*F;X6Q:8*B&gt;'^S4G^U;7:J9W&amp;U;7^O=SZM&gt;GRJ9C"3:7NV='6S982P=EZP&gt;'FG;7.B&gt;'FP&lt;H-O&lt;(:D&lt;'&amp;T=Q!=5G6L&gt;8"F=G&amp;U&lt;X*/&lt;X2J:GFD982J&lt;WZT)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!&gt;1!I!&amp;U^V&gt;(.J:'5A97FS)(2F&lt;8!A7W2F:U.&gt;!"V!#A!75X6Q='RZ)'&amp;J=C"U:7VQ)&amp;NE:7&gt;$81!!(5!+!"&gt;&amp;?(2S97.U)'&amp;J=C"U:7VQ)&amp;NE:7&gt;$81!@1!I!'%^V&gt;'&gt;P;7ZH)'&amp;J=C"U:7VQ)&amp;NE:7&gt;$81!!&amp;5!'!!Z'97YA5'6S9W6O&gt;'&amp;H:1!!'U!7!!5"-!%R!4)"-Q%U!!F'97YA=X2B:W5!(E!B'%*Z='&amp;T=S!I6$V0='6O,#"'07.M&lt;X.F+1!!-5!7!!)*186U&lt;WVB&gt;'FD"EVB&lt;H6B&lt;!!!&amp;UVP:'5A+$!^986U&lt;SQA-4VN97ZV97QJ!#"!5!!)!!=!#!!*!!I!#Q!-!!U!$AB"&lt;'QA:'&amp;U91!!:E"Q!"Y!!%%?5G6L&gt;8"F=G&amp;U&lt;X*/&lt;X2J:GFD982J&lt;WZT,GRW&lt;'FC)&amp;*F;X6Q:8*B&gt;'^S4G^U;7:J9W&amp;U;7^O=SZM&gt;G.M98.T!"N3:7NV='6S982P=EZP&gt;'FG;7.B&gt;'FP&lt;H-A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!0!"!#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!#!!!!*!!!!!!!1!2!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
</LVClass>
